# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

from .MC20 import MC20a, MC20d, MC20e

__all__ = ['MC20a', 'MC20d', 'MC20e']
